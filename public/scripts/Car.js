class Car extends Component {
  static list = [];

  static init(cars) {
    this.list = cars.map((car) => new this(car));
  }

  constructor(props) {
    super(props);
    let { id, plate, manufacture, model, image, rentPerDay, capacity, description, transmission, available, type, year, options, specs, availableAt } = props;
    this.id = id;
    this.plate = plate;
    this.manufacture = manufacture;
    this.model = model;
    this.image = image;
    this.rentPerDay = rentPerDay;
    this.capacity = capacity;
    this.description = description;
    this.transmission = transmission;
    this.available = available;
    this.type = type;
    this.year = year;
    this.options = options;
    this.specs = specs;
    this.availableAt = availableAt;
  }
  render() {
    return `
          <div class="col-lg-4">
          <div class="card-mobil">
              <img class="img-fluid img-mobil" src="${this.image}" alt="">
              <div class="keterangan-mobil">
                  <p class="tipe">${this.manufacture}/${this.model}</p>
                  <h3 class="title">${this.rentPerDay}</h3>
                  <p class="keterangan"> Deskirpsi </p>
              </div>
              <div class="spesifikasi-mobil row">
                  <div class="spesifikasi-detail d-flex justify-content-start">
                      <img class="spesifikasi-icon" src="assets/fi_userstipis.png" alt="">
                      <p class="spes-text">${this.capacity}</p>
                  </div>
                  <div class="spesifikasi-detail d-flex justify-content-start">
                      <img class="spesifikasi-icon" src="assets/fi_settings.png" alt="">
                      <p class="spes-text">${this.transmission}</p>
                  </div>
                  <div class="spesifikasi-detail d-flex justify-content-start">
                      <img class="spesifikasi-icon" src="assets/fi_calendartipis.png" alt="">
                      <p class="spes-text">Tahun ${this.year}</p>
                  </div>
              </div>
              <div class="d-grid mt-2">
                  <a href="#" class="btn btn-block main__cta">Pilih Mobil</a>
              </div>
          </div>
      </div>
    `;
  }
}

 

